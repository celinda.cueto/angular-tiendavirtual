import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InferiorBarComponent } from './inferior-bar.component';

describe('InferiorBarComponent', () => {
  let component: InferiorBarComponent;
  let fixture: ComponentFixture<InferiorBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InferiorBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InferiorBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
