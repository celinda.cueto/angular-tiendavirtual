import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { ProductsComponent } from './products/products.component';
import { NewProductsComponent } from './new-products/new-products.component';
import { HomeComponent } from './home/home.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { InferiorBarComponent } from './inferior-bar/inferior-bar.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { AboutComponent } from './about/about.component';
import { ListProductsComponent } from './list-products/list-products.component';
import { ContactComponent } from './contact/contact.component';
import { NuevosprodComponent } from './nuevosprod/nuevosprod.component';
import { UsadosprodComponent } from './usadosprod/usadosprod.component';
import { AccesoriosComponent } from './accesorios/accesorios.component';
import { GamerComponent } from './gamer/gamer.component';

@NgModule({
  declarations: [
    AppComponent,
     MenuBarComponent,
    ProductsComponent,
    NewProductsComponent,
    HomeComponent,
    SearchBarComponent,
    InferiorBarComponent,
    ServiciosComponent,
    AboutComponent,
    ListProductsComponent,
    ContactComponent,
    NuevosprodComponent,
    UsadosprodComponent,
    AccesoriosComponent,
    GamerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
