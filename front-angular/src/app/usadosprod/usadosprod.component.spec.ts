import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsadosprodComponent } from './usadosprod.component';

describe('UsadosprodComponent', () => {
  let component: UsadosprodComponent;
  let fixture: ComponentFixture<UsadosprodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsadosprodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsadosprodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
