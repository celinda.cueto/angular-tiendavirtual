import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevosprodComponent } from './nuevosprod.component';

describe('NuevosprodComponent', () => {
  let component: NuevosprodComponent;
  let fixture: ComponentFixture<NuevosprodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevosprodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevosprodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
