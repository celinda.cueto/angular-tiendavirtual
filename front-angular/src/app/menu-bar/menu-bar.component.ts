import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ProductsComponent } from '../products/products.component';

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.css']
})
export class MenuBarComponent implements OnInit {
  @ViewChild(ProductsComponent)
  products: ProductsComponent = new ProductsComponent;
  constructor() { }

  ngOnInit(): void {
  }
  enviarMensaje() {
  this.products.saludo("Hola hijo");
}

}
