import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { ListProductsComponent } from './list-products/list-products.component';
import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { NewProductsComponent } from './new-products/new-products.component';
import { ProductsComponent } from './products/products.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ServiciosComponent } from './servicios/servicios.component';


const routes: Routes = [
  
  { path: 'menu-bar', component: MenuBarComponent},
  { path: 'products', component: ProductsComponent},
  { path: 'new-products', component: NewProductsComponent},
  { path: 'home', component: HomeComponent},
    { path: 'servicios', component: ServiciosComponent},
    { path: 'about', component: AboutComponent},
  { path: 'search-bar', component: SearchBarComponent},
  {path:'inferior-bar',component:SearchBarComponent},
  {path:'list-products',component:ListProductsComponent},
  {path:'contact',component:ContactComponent}

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
